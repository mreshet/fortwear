var colors = require('colors');
module.exports = function(grunt) {
  "use strict";

  // Project configuration
  grunt.initConfig({
    // Read package.json Metadata.
    pkg: grunt.file.readJSON('package.json'),
    banner: '/*******************************************************************************\n' +
            ' *              <%= pkg.name %> (Version <%= pkg.version %>)\n' +
            ' *      Author: <%= pkg.author %>\n' +
            ' *  Created on: <%= grunt.template.today("mmmm dd,yyyy") %>\n' +
            ' *     Project: <%= pkg.name %>\n' +
            ' *******************************************************************************/\n',
    less: {
      rtl: {
        options: {
			strictMath: true,
			cleancss: false
		},
		files: {
			'dist/css/<%= pkg.name %>.css': ['../bootstrap-rtl/bootstrap/less/bootstrap.less', 'less/theme.less'],
			'dist/css/<%= pkg.name %>-rtl.css': ['../bootstrap-rtl/bootstrap/less/bootstrap.less', '../bootstrap-rtl/less/bootstrap-rtl.less', 'less/theme-rtl.less'],
        }
      },
      minify: {
		options: {
			cleancss: true
        },
        files: {
			'../../wp-content/themes/fortweartheme/assets/css/<%= pkg.name %>.min.css': 'dist/css/<%= pkg.name %>.css',
			'../../wp-content/themes/fortweartheme/assets/css/<%= pkg.name %>-rtl.min.css': 'dist/css/<%= pkg.name %>-rtl.css'
        }
      }
    },

    usebanner: {
        options: {
          position: 'top',
          banner: '<%= banner %>',
          linebreak: true

        },
        files: {
          src: [
              'dist/css/<%= pkg.name %>.css', 'dist/css/<%= pkg.name %>.min.css',
              'dist/css/<%= pkg.name %>-rtl.css', 'dist/css/<%= pkg.name %>-rtl.min.css'
          ]
        }
    },

  watch: {
      scripts: {
          files: ['less/*.less'],
          tasks: ['less'],
          options: {
              spawn: false
          }
      }
  }
  });
	
grunt.event.on('watch', function(action, filepath, target) {
	console.log('\n\n\nFortWear\n\n\n'.blue.bgWhite);
});


// Load uglify plugin
grunt.loadNpmTasks('grunt-contrib-less');
grunt.loadNpmTasks('grunt-banner');
grunt.loadNpmTasks('grunt-contrib-watch');

// Default Task
grunt.registerTask('default', ['less', 'usebanner']);
};