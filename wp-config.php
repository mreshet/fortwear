<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'fortwear');

/** MySQL database username */
define('DB_USER', 'fortwear');

/** MySQL database password */
define('DB_PASSWORD', '1q2w3e4r5t6y');

/** MySQL hostname */
define('DB_HOST', 'localhost:3308');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'cWDvnMLJ0en<:G$Ap,D_cru*<E~x/k:Ht]=EG_^-nIK6>CM`+6;A`3/<VOIVM9 &');
define('SECURE_AUTH_KEY',  '^Dc9l*b:tp-iX<`m$<W5>gSjx7(^w;3D=Qo:HZ~[jJ/r1#TPJ#tqOo_eFEO3NzVR');
define('LOGGED_IN_KEY',    '%5XDY5}oJOk418/3]Awl:a?NrvEB6=V4EFe?3-%yh]p]vg>$ax[ZUMlCRia&0!,i');
define('NONCE_KEY',        '# {G(+kD!VWPFJz|P@1Y%V44?+)LGc-T%9BSDKg>^C!SK##Gr-Hs-;[8F(:d,E=M');
define('AUTH_SALT',        'R&/eube=8k>,6c93;Oy3h(tB4KP0wk;z.<p=pi7N/:pF-CJi$EEq-_A>X>{(q=9&');
define('SECURE_AUTH_SALT', 'qqsG9H~i@;WAkYjX!D{W<b)HJ#|-#}{j$JE]+8BPFLjLy.dpd`@ vt6}&R%46v)c');
define('LOGGED_IN_SALT',   '-VWEt.xwi:e^f)Fd9.:TtUD}]h]keB]1ARIFi#fMZ5@tb.QEBj.vOI^:h&PF+u]A');
define('NONCE_SALT',       'aH)tNRRCJCNw|bd7Twvx2,u@_o%WL1@Bl*foP)nDQc*9gx7^ }6$N}iet?{2,yuy');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
