<?php get_template_part('templates/head'); ?>

<body <?php body_class(); ?>>
    <div id="wrapper">
        <?php /* TOP BAR */
            get_template_part('templates/topbar'); ?>

        <?php /* uncomment for debugging purposes only:
        <code style="position:absolute;opacity:.2">&nbsp;<b>Template:</b> <?php echo strchr(kadence_template_path(), '/'); ?>&nbsp;</code>
        */ ?>

        <?php //do_action('get_header');
            get_template_part('templates/header'); ?>

        <div class="wrap contentclass" role="document">
            <?php do_action('kt_afterheader'); ?>

            <?php include kadence_template_path(); ?>
        </div><!-- /.wrap -->

        <?php /* PAGE BOTTOM */
            get_template_part('templates/bottom'); ?>

    </div><!-- /#wrapper -->

    <?php //do_action('get_footer');
      get_template_part('templates/footer'); ?>

</body>
</html>

