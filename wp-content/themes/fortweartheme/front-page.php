<?php  global $virtue;

	/**
	 * MReshet @ 04/06/2015
	 * Don't display sliders and page title
	 **************************************
	if (isset($virtue['mobile_switch'])) {
		$mobile_slider = $virtue['mobile_switch'];
	} else {
		$mobile_slider = '0';
	}
	if(isset($virtue['choose_slider'])) {
		$slider = $virtue['choose_slider'];
	} else {
		$slider = 'mock_flex';
	}
	if($mobile_slider == '1') {
		$mslider = $virtue['choose_mobile_slider'];
		if ($mslider == "flex") {
			get_template_part('templates/mobile_home/mobileflex', 'slider');
		} else if ($mslider == "video") {
			get_template_part('templates/mobile_home/mobilevideo', 'block');
		}
	}
	if ($slider == "flex") {
			get_template_part('templates/home/flex', 'slider');
	} else if ($slider == "thumbs") {
			get_template_part('templates/home/thumb', 'slider');
	} else if ($slider == "latest") {
			get_template_part('templates/home/latest', 'slider');
	} else if ($slider == "carousel") {
			get_template_part('templates/home/carousel', 'slider');
	} else if ($slider == "video") {
			get_template_part('templates/home/video', 'block');
	} else if ($slider == "mock_flex") {
			get_template_part('templates/home/mock', 'flex');
	}

	$show_pagetitle = false;
	if(isset($virtue['homepage_layout']['enabled'])){
		$i = 0;
		foreach ($virtue['homepage_layout']['enabled'] as $key=>$value) {
			if($key == "block_one") {
				$show_pagetitle = true;
			}
			$i++;
			if($i==2) break;
		}
	}
	if ($show_pagetitle == true) { ?>
		<div id="homeheader" class="welcomeclass">
			<div class="container">
				<?php get_template_part('templates/page', 'header'); ?>
			</div>
		</div><!--titleclass-->
	<?php } */ ?>

	<div id="homeslider" class="container homepageslider">
		<div class="row">
			<div class="shape col-md-7">
				<?php get_template_part('templates/content', 'page'); /* HOME PAGE */ ?>
			</div>
			<div class="slider col-md-5">
				<div class="slider-inner">
					<?php get_template_part('templates/home/home', 'slider'); /* HOME PRODUCT SLIDER */ ?>
				</div>
			</div>
		</div>
	</div>

    <div id="content" class="container homepagecontent">
   		<div class="row">
          	<div class="main <?php echo esc_attr(kadence_main_class()); ?>" role="main">
          		<div class="entry-content" itemprop="mainContentOfPage">

      		<?php
				if (isset($virtue['homepage_layout']['enabled'])) {
      				$layout = $virtue['homepage_layout']['enabled'];
      		  	} else {
      		  		$layout = array("block_twenty" => "block_twenty", "block_five" => "block_five");
      		  	} ?>

			<?php /* if ($layout):

				    	echo 'block_one<hr />';
						   	if ($show_pagetitle == false) {?>
					    	  <div id="homeheader" class="welcomeclass">
									<?php get_template_part('templates/page', 'header'); ?>
								</div><!--titleclass-->
							<?php }


						echo 'block_four<hr />';
							if (is_home()) {
									if(kadence_display_sidebar()) {
										$display_sidebar = true;
										$fullclass = '';
									} else {
										$display_sidebar = false;
										$fullclass = 'fullwidth';
									}
									if(isset($virtue['home_post_summery']) and ($virtue['home_post_summery'] == 'full')) {
										$summery = "full";
										$postclass = "single-article fullpost";
									} else {
										$summery = "summery";
										$postclass = "postlist";
									} ?>
								<div class="homecontent <?php echo esc_attr($fullclass); ?>  <?php echo esc_attr($postclass); ?> clearfix home-margin"> 
									<?php while (have_posts()) : the_post(); 
								  			if($summery == 'full') {
												if($display_sidebar){
													get_template_part('templates/content', 'fullpost'); 
												} else {
													get_template_part('templates/content', 'fullpostfull');
												}
											} else {
												if($display_sidebar){
												 	get_template_part('templates/content', get_post_format()); 
												 } else {
												 	get_template_part('templates/content', 'fullwidth');
												 }
											}
										endwhile; 
									
										//Page Navigation
								        if ($wp_query->max_num_pages > 1) :
								        	virtue_wp_pagenav();
								        endif; ?>
								</div> 
							<?php } else { ?>
								<div class="homecontent clearfix home-margin"> 
									<?php get_template_part('templates/content', 'page'); ?>
								</div>
							<?php }

						echo 'block_five<hr />';
							get_template_part('templates/home/blog', 'home');

						echo 'block_six<hr />';
							get_template_part('templates/home/portfolio', 'carousel');

						echo 'block_seven<hr />';
							get_template_part('templates/home/icon', 'menu');

						echo 'block_twenty<hr />';
							get_template_part('templates/home/icon', 'menumock');

				endif; */ ?>

				</div>
			</div><!-- /.main -->
		</div>
	</div>

	<section id="armortypes">
		<div class="container container-fullwidth">
			<div class="row">
			<?php
				/** MReshet: Render armor types on home page (four clickable images) */
				get_template_part('templates/home/portfolio', 'carousel');
			?>
			</div>
		</div>
	</section>
