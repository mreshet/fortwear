<?php global $virtue; ?>
<div id="contactmap" class="clearfix">
    <div class="container container-fullwidth">
        <div class="row text-center vertical-align">
            <div class="col-xs-12 clearfix">
                <div class="column-inner">
                    <p><a href="mailto:info@fortwear.com"><b>info@fortwear.com</b></a></p>
                    <br /><br />
                    <a target="_self" class="kad-btn kad-btn-primary" href="./contact-us">Contact Us</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="footercolumns clearfix">
    <div class="container container-fullwidth">
        <div class="row">
            <div class="col-md-4 clearfix">
                <div class="footersocial">
                    <?php if (has_nav_menu('topbar_navigation')) :
                        /* Use topbar navigation menu */
                        wp_nav_menu(array('theme_location' => 'topbar_navigation', 'menu_class' => 'sf-menu'));
                    endif; ?>
                </div>
            </div>
            <div class="col-md-4 text-center clearfix">
                <a class="footerlogo" href="<?php echo home_url(); ?>/" title="<?php  echo bloginfo('name'); ?>"> </a>
            </div>
            <div class="col-md-4 clearfix">
                <?php if (has_nav_menu('footer_navigation')) :
                    ?><div class="footernav clearfix"><?php
                    wp_nav_menu(array('theme_location' => 'footer_navigation', 'menu_class' => 'footermenu'));
                    ?></div><?php
                endif;?>
            </div>
        </div>
    </div>
</div>

<div class="stickyfooterfix"> </div>