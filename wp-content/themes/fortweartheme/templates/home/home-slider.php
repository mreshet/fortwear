<div class="sliderclass carousel_outerrim kad-desktop-slider">
    <?php global $virtue;
    /* SLIDER SIZE */
    $slidewidth = 675;
    $slideheight = 480;

    if(isset($virtue['slider_captions'])) {
        $captions = $virtue['slider_captions'];
    } else {
        $captions = '';
    }
    if(isset($virtue['home_slider'])) {
        $slides = $virtue['home_slider'];
    } else {
        $slides = '';
    }
    if(isset($virtue['slider_autoplay']) && $virtue['slider_autoplay'] == 0) {
        $autoplay = 'false';
    } else {
        $autoplay = 'true';
    }
    if(isset($virtue['slider_pausetime'])) {
        $pausetime = $virtue['slider_pausetime'];
    } else {
        $pausetime = '6000';
    } ?>

    <div id="imageslider" class="loading">
        <div class="carousel_slider_outer fredcarousel fadein-carousel" style="overflow:hidden; max-width:<?php echo esc_attr($slidewidth);?>px; height:<?php echo esc_attr($slideheight);?>px; margin-left: auto; margin-right:auto;">
            <div class="carousel_slider initcarouselslider" data-carousel-container=".carousel_slider_outer" data-carousel-transition="600"
                 data-carousel-height="<?php echo esc_attr($slideheight); ?>" data-carousel-auto="<?php echo esc_attr($autoplay); ?>"
                 data-carousel-speed="100<?php echo esc_attr($pausetime); ?>" data-carousel-id="carouselslider">
                <?php $args = array(
                    'post_type' => 'product',
                    'posts_per_page' => 8
                );
                $loop = new WP_Query( $args );
                if ( $loop->have_posts() ) {
                    while ( $loop->have_posts() ) : $loop->the_post();
                        global $product;

                        //wc_get_template_part( 'content', 'product' );
                        //wc_get_template_part( 'content', 'single-product' );

                    ?>
                <div class="carousel_gallery_item" style="float:left; display:table; position:relative; margin:0; width:auto; height:<?php echo esc_attr($image[2]); ?>px">
                    <div class="carousel_gallery_item_inner" style="vertical-align:middle;display:table-cell">
                        <a class="flex-link" href="<?php echo esc_url( get_permalink( $product->id ) ); ?>" title="<?php echo esc_attr( $product->get_title() ); ?>">
                            <?php echo $product->get_image( 'large', array('class' => 'flex-image') ); ?>
                            <div class="flex-caption">
                                <div class="captionbase">
                                    <p class="captiontitle text-uppercase"><?php echo $product->get_title(); ?></p>
                                    <p class="captiontext">Safe series</p>
                                    <?php
                                        $id = get_the_ID();
                                        $armortype = get_post_meta( $id, 'armortype', true ); // concealed, external/overt
                                        $protection = get_post_meta( $id, 'protection', true ); // 1A, 2A, 3A, 4A
                                    ?>
                                    <p class="captionicons">
                                        <span class="icon-armortype <?php if ($armortype == 'overt' || $armortype == 'external') echo 'overt'; ?>" title="<?php echo ucfirst($armortype); ?>"></span>
                                    <?php switch ($protection):
                                        case '1A':
                                        case 'light':
                                            ?>
                                            <span class="icon-protection-pistol" title="Pistol"></span>
                                            <?php break;
                                        case '2A':
                                            ?>
                                            <span class="icon-protection-pistol" title="Pistol"></span>
                                            <span class="icon-protection-pistol" title="Pistol"></span>
                                            <?php break;
                                        case '3A':
                                            ?>
                                            <span class="icon-protection-pistol" title="Pistol"></span>
                                            <?php break;
                                        case '4A':
                                            ?>
                                            <span class="icon-protection-pistol" title="Pistol"></span>
                                            <?php break;
                                        default:
                                            ?>
                                            <span class="icon-protection" title="<?php echo $protection; ?>"><?php echo $protection; ?></span>
                                            <?php break;
                                    endswitch; ?>
                                    </p>
                                </div>
                                <button class="btn btn-details text-uppercase"><?php echo __( 'Details' ); ?></button>
                            </div>
                        </a>
                    </div>
                </div>
                <?php
                    endwhile;
                } else {
                    echo __( 'No products found' );
                }
                wp_reset_postdata();
                ?>

                <?php /* foreach ($slides as $slide) :
                if (!empty($slide['target']) && $slide['target'] == 1) {$target = '_blank';} else {$target = '_self';}
                $image = aq_resize($slide['url'], null, $slideheight, false, false);
                if(empty($image)) {$image = array($slide['url'],$slidewidth,$slideheight);}
                echo '<div class="carousel_gallery_item" style="float:left; display: table; position: relative; text-align: center; margin: 0; width:auto; height:'.esc_attr($image[2]).'px;">';
                echo '<div class="carousel_gallery_item_inner" style="vertical-align: middle; display: table-cell;">';
                if($slide['link'] != '') echo '<a href="'.esc_url($slide['link']).'" target="'.esc_attr($target).'">';
                echo '<img src="'.esc_url($image[0]).'" width="'.esc_attr($image[1]).'" height="'.esc_attr($image[2]).'" />';
                if ($captions == '1') { ?>
                    <div class="flex-caption">
                        <?php if ($slide['title'] != '') echo '<div class="captiontitle headerfont">'.$slide['title'].'</div>';
                        if ($slide['description'] != '') echo '<div><div class="captiontext headerfont"><p>'.$slide['description'].'</p></div></div>';?>
                    </div>
                <?php }
                if($slide['link'] != '') echo '</a>'; ?>
                    </div>
                </div>
                <?php endforeach; */ ?>
            </div>
            <div class="clearfix"></div>
            <a id="prevport-carouselslider" class="kad-btn prev_carousel" href="#"></a>
            <a id="nextport-carouselslider" class="kad-btn next_carousel" href="#"></a>
        </div> <!--fredcarousel-->
    </div><!--Container-->
</div><!--sliderclass-->