<?php global $virtue; ?>
<?php if (kadence_display_topbar()) : ?>
  <div id="topbar" class="topclass">
    <div class="container container-fullwidth">
      <div class="row">
        <div class="col-md-6 col-sm-6">
          <div class="topbar-widget">
            <a id="toplogo" href="<?php echo home_url(); ?>/" title="<?php bloginfo('name');?>">
              <p class="toplogo-image"></p>
              <p class="toplogo-text">A lot more than protection.</p>
            </a>
          </div> <!-- Close #logo -->
          <div id="topbar-search" class="topbar-widget pull-left">
            <?php if(kadence_display_topbar_widget()) { if(is_active_sidebar('topbarright')) { dynamic_sidebar('topbarright'); }
              } else { if(kadence_display_top_search()) {get_search_form();}
          } ?>
          </div> <!-- Close #topbar-search -->
        </div> <!-- close col-md-6-->
        <div class="col-md-6 col-sm-6">
          <div class="topbarmenu pull-right clearfix">
            <?php if (has_nav_menu('topbar_navigation')) :
              wp_nav_menu(array('theme_location' => 'topbar_navigation', 'menu_class' => 'sf-menu pull-right'));
            endif; ?>
            <?php if(kadence_display_topbar_icons()) : ?>
              <div class="topbar_social">
                <ul class="pull-right">
                  <?php $top_icons = $virtue['topbar_icon_menu'];
                  foreach ($top_icons as $top_icon) {
                    $target = ( !empty($top_icon['target']) && $top_icon['target'] == 1 ) ? '_blank' : '_self';
                    echo '<li><a href="'.esc_url($top_icon['link']).'" target="'.esc_attr($target).'" title="'.esc_attr($top_icon['title']).'" data-toggle="tooltip" data-placement="bottom" data-original-title="'.esc_attr($top_icon['title']).'">';
                    if (!empty($top_icon['url'])) {
                      echo '<img src="'.esc_url($top_icon['url']).'"/>' ;
                    } else {
                      echo '<i class="'.esc_attr($top_icon['icon_o']).'"></i>';
                    }
                    echo '</a></li>';
                  } ?>
                </ul>
              </div>
            <?php endif; ?>
            <?php if(isset($virtue['show_cartcount'])) {
              if($virtue['show_cartcount'] == '1') {
                if (class_exists('woocommerce')) {
                  global $woocommerce; ?>
                  <ul class="kad-cart-total pull-right">
                    <li>
                      <a class="cart-contents" href="<?php echo esc_url($woocommerce->cart->get_cart_url()); ?>" title="<?php esc_attr_e('View your shopping cart', 'woocommerce'); ?>">
                        <i class="icon-shopping-cart" style="padding-right:5px;"></i> <?php _e('Your Cart', 'virtue');?> <span class="kad-cart-dash">-</span> <?php echo $woocommerce->cart->get_cart_total(); ?>
                      </a>
                    </li>
                  </ul>
                <?php } } }?>
          </div>
        </div><!-- close col-md-6 -->
      </div> <!-- Close Row -->
    </div> <!-- Close Container -->
  </div>
<?php endif; ?>
